<form method="POST">
    <div>
        <label>Email</label> <br>
        <input type="email" name="email">
    </div>

    <div>
        <label>Nama</label> <br>
        <input type="text" name="nama">
    </div>

    <div>
        <label>Usia</label> <br>
        <input type="number" name="usia">
    </div>

    <div>
        <label>Tanggal Lahir</label> <br>
        <input type="date" name="tanggal_lahir">
    </div>
    
    <div style="margin-bottom: 1rem;">
        <label>Jenis Kelamin</label> <br>
        <input type="radio" name="jenis_kelamin" value="l"> Laki-Laki <br>
        <input type="radio" name="jenis_kelamin" value="p"> Perempuan
    </div>

    <div style="margin-bottom: 1rem;">
        <label>Bidang Pilihan</label> <br>
        <select name="Bidang Pilihan">
            <option value="Humas">Humas</option>
            <option value="Minat Bakat">Minat Bakat</option>
            <option value="Agama">Agama</option>
            <option value="Kemahasiswaan">Kemahasiswaan</option>
        </select>
    </div>

    <div style="margin-bottom: 1rem;">
        <label>Bakat Keterampilan</label> <br>
        <input type="checkbox" name="bakat[]" value="menulis"> menulis <br>
        <input type="checkbox" name="bakat[]" value="fotografi"> fotografi <br>
        <input type="checkbox" name="bakat[]" value="public speaking"> public speaking <br>
        <input type="checkbox" name="bakat[]" value="olahraga"> olahraga <br>
    </div>

    <div>
        <label>Alasan Mengikuti Organisasi</label> <br>
        <textarea name="alasan"></textarea>
    </div>
    <div>
        <button>Submit</button>
    </div>
</form>